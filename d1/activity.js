let http = require('http'); 
let port = 4000;
let loginPage = "/login";
let status = {
	success: 200,
	error: 404
}

let server = http.createServer(function(request, response){

	if (request.url == loginPage) {
		response.writeHead(status.success, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page');
		console.log(`> User is accessing : http://localhost${request.url}.\n> Port: ${port}\n> Status code: ${status.success}\n`); 
	} else {
		response.writeHead(status.error, {'Content-Type': 'text/plain'});
		response.end('I\'m sorry, the page that you\'re looking for is not found.');
		console.log(`> User is requesting a none existing page.\n> Url: http://localhost${request.url}\n> Port: ${port}\n> Status code: ${status.error}\n`);
	}

}).listen(port);
 

server;

console.log(`
> Server is running at: http://localhost:${port} \n 
`)